# Kube guide:

Kubernetes是一个基于容器技术的分布式架构解决方案, 我们可以把Kubernetes看做Docker的上层架构, 他具有完备的集群管理能力, 包括多层次的安全防护和准入机制, 多租户应用支撑能力, 透明的服务注册和服务发现机制, 内建智能负载均衡, 强大的故障发现和自我修复, 服务滚动升级和在线扩容能力, 可扩展的资源自动调度机制,以及多粒度的资源配额管理能力. 


## Master:
集群控制节点, 独立的集群首脑, 控制所有node, 保存所有资源的数据在etcd中

#### 1.关键进程:
- Kube-apiserver: 提供HTTP Rest接口
- kube-controller-manager: Kube所有资源的自动化控制中心
- kube-scheduler: 资源(Pod)调度中心


## Node:
集群中的工作负载节点, 节点会动态故障转移.

#### 1.关键进程:
- Kubelet: 负责Pod对应容器的创建,启停工作.
- Kube-proxy: 实现kube service 通信与负载均衡.
- Docker Engine(docker): Docker引擎, 负责容器的创建和管理.

#### 2.CMD:
- 查看node个数
```
kubectl get nodes
```

- 查看node details info
```
kubectl describe node
```

## Pod:
Pause根容器+若干业务容器, 容器间共享根容器IP及其挂载的Volume, Pod间的容器能够互相通信.

#### 1.类型:
- 普通Pod: 存放在Master的etcd内.
- 静态Pod(static): 存放在某个具体node的文件中且与其绑定.

#### 2.特点:
- Pod的中的容器停止后, kube会自动检测到这个问题后重启这个Pod.
- Pod所在的Node宕机, kube会将这个Node下面的所有Pod调度到其他Node.

## Label:
key=value键值对, 可以附加到各种资源上, Node, Pod, Service, RC等

#### 1.特点:
- 通过对指定资源捆绑Lable实现资源分组管理.
- 被标签的资源可以通过Label Selector查询筛选特定的资源对象
- 分组管理实现了整个集群的高可用性

## Replication controller(RC):
声明Pod在任意时刻都符合replicas期望值.

#### 1.包含:
- Pod期待的副本数(replicas)
- 用于筛选目标Pod的Label Selector.
- 当Pod副本数小于预期, 用于创建新的Pod模板.

#### 2.特点与作用:
- 可以通过定义一个RC实现Pod的创建过程及副本数量的自动控制.
- RC包含完整的Pod定义模板.
- RC通过Lable Selector机制实现对Pod副本自动控制.
- 通过改变RC的replicas实现Pod扩容和缩放.
- 通过改变RC的镜像版本实现Pod滚动升级.

#### 2.CMD:
通过修改replicas, 实现Pod动态缩放
```
kubectl scale rc redis-slave --replicas=3
```

## Deployment
RC的升级概念, 使用Replica Set解决Pod的编排问题, 随时监控当前Pod的部署进度,格式与RC类似.

#### 1.场景:
- 创建一个Deployment对象来生成对应的Replica Set, 并完成Pod副本创建.
- 检查Deployment的状态(Pod replicas是否达到预期值)来判断部署是否完成.
- 更新Deployment(镜像升级)以创建新的Pad.
- 当前Deployment不稳定则roll back早先版本.
- 挂起或恢复一个Deployment.

#### 2.CMD:
创建Deployment
```
kubectl create -f tomcat-deployment.yaml
```
查看Deployment信息
```
kubectl get deployments
```
查看Replica Set
```
kubectl get rs
```
查看Pod状态
```
kubectl get pods
```

## Horizontal Pod Autoscaler
在Deployment的基础上的升级, 一种Kube的资源对象, 通过追踪分析RC控制的所有目标Pod的负载来动态调整Pod副本数

#### 1.度量指标
- CPU Utilization Percentage.
- 应用自定义指标, 例如服务每秒响应请求书(TPS或QPS)

## Service
Service定义了一个服务的访问入口地址, 前端应用Pod通过这个入口地址访问其背后的一组由Pod副本组成的集群实例.

#### 1.特点
- 所有服务通过Kube Service微服务单元组成, 服务之间通过TCP/IP进行通信
- 多个Pod的End point(Pod IP+Container Port)通过加入负载均衡器转发列表,实现Pod集群服务
- 每个Node的Kube-proxy作为负载均衡器负责把Service请求转发到某个Pod实例上.
- 通过Linux环境变量实现Service名对应Cluster IP

#### 2. CMD
查看Service对应Endpoint列表, ENDPOINTS对应Pod的IP地址,端口为Container端口.
```
Kubectl get endpoints
```
查看Cluster IP
```
kubectl get svc myweb -o yaml
```

#### 3.外部访问Service
- Node IP: 节点的物理网卡IP, Kube集群之外的节点访问集群内某个节点必须要通过Node IP进行通信.
- Pod IP: 每个Pod的IP地址, 通过Docker Engine上的Docker0二层网桥将位于不同Node上的Pod连接起来进行彼此通信, 最后实际的流量会通过Node IP流出.
- Cluster IP: Service虚拟出来的一个内部IP地址, 无法进行集群外部访问
  - 仅作用于Service对象, 并由Kube进行IP管理和分配
  - 无法被Ping, 因为没有一个"实体网络对象"来响应
  - 仅结合Service Port组成一个具体的通信端口, 其独立属于Kube集群的一个封闭空间, 不具备TCP/IP通信能力, 如果要访问需要做额外配置
  - Node IP, Pod IP, Cluster IP采用于不同于我们熟知的IP路由的特殊路由算法
  - 如果该Cluster IP为web服务模块需要开放给外部访问, 需要做如下扩展

  ```yaml
  apiVersion: v1
  kind: Service
  metadata:
    name: tomcat-service
  spec:
    type: NodePort
    ports:
     - port: 8080
       nodePort: 31002
     selector: 
       tier: frontend 
  ```

  nodePort:31002这个属性表明我们手动指定一个tomcat-service的外部可访问端口

- NodePort会在每个Node上为需要外部访问的service开启一个对应的TCP端口监听, 访问的FQDN为"Node IP + NodePort"

## Volume(存储卷)
Volumn是多个在Pod下的容器的共享目录, 它定义在Pod中并被Pod里多个容器挂载到具体目录下, 它与Pod的生命周期相同, 与Pod中的容器生命周期不相关, 当容器终止或重启, Volumn中数据不会丢失.

#### Example
给Tomcat Pod增加一个名字为dataVol的Volumn, 并mount到容器的/mydata-data

```yaml
template:
  metadata:
    labels:
      app: app-demo
      tier: frontend
    spec:
      volumes:
        - name: datavol
          emptyDir: {}
      containers:
      - name: tomcat-demo
        image: tomcat
        volumeMounts:
          - mountPath: /mydata-data
            name: datavol
        imagePullPolicy: IfNotPresent
```

#### Concept
- emptyDir: 在Pod分配到Node时创建, 初始值为空, Kube会自动分配一个目录, 无需指定. 当Pod从Node上移除时, emptyDir的数据会被永久删除
  - 某些应用程序的临时空间
  - 长时间任务的中间过程CheckPoint的品势保存目录
  - 一个容器需要从另外一个容器获取数据的目录(多容器共享目录) 
  - 未来可以设置emptyDir的介质(机械硬盘, 固态硬盘, 内存tmpfs)

- hostPath: 在Pod上挂载宿主机器上的文件或目录
  - 容器应用程序生成的日志可以使用宿主机进行永久存储.
  - 需要访问宿主机上Docker引擎内部数据结构的容器应用, 可以通过定义hostPath为宿主/var/lib/docker, 使容器内部应用直接访问Docker文件系统.
  - 不同Node下具有相同配置的Pod需要保证所在宿主机的目录结构一致.
  - hostPath在宿主机上的目录无法生效宿主机所在资源配额配置.

```yaml
volumes:
- name: "persistent-storage"
  hostPath:
    path: "/data"
```

- gcePersistentDisk: 表示使用谷歌公有云提供的永久磁盘(Persistent Disk, PD)存放Volumn数据, 他与EmptyDir不同, PD上的内容会被永久保存, 当Pod被删除时, PD只是被卸载(Umount), 不会被删除, 需要先创建一个永久磁盘(PD), 才能使用gcePersistentDisk
 - Node需要是GCE虚拟机
 - 这些虚拟机需要与PD存在于相同的GCE项目和Zone中.

创建一个PD:

```shell
gcloud compute disk create --size=500G --zone=us-centrall-a my-data-disk
```

定义gcePersistentDisk类型的Volumn:

```yaml
volumns:
- name: test-volumn
  # This GCE PD must already exist.
  gcePersistentDisk:
    pdName: my-data-disk
    fsType: ext4
```

- awsElasticBlockStore: 表示使用亚麻徐公有云提供的EBS Volume存储数据
  - Node需要时AWS EC2的实例
  - 这些实例需要与EBS volume存在于相同的region和availablity-zone
  - EBS只支持单个EC2实例mount一个volume.

创建一个EBS volume:

```shell
aws ec2 create-volume --availibility-zone eu-west-la --size 10 --volume-type gp2
```

定义awsElasticBlockStore类型的Volume的示例如下:

```yaml
volumes:
- name: test-volume
  # This AWS EBS volume must already exist
  awsElasticBlockStore:
    volumeID: aws://<availibility-zone>/<volume-id>
    fsType: ext4
```

kubectl create secret generic mysql-pass --from-literal='password=countonme'
